#include "lmtk_example.h"

#include <lmtk/text_layout.h>

class lmtk_text_layout_example : public lmtk_example
{
  public:
    lmtk_text_layout_example()
        : lmtk_example{},
          font_loader{lmtk::create_font_loader()},
          font{
            font_loader->create_font(lmtk::font_init{
              .typeface_name = "Arial",
              .pixel_size = 32,
            }),
          },
          text_layout{
            lmtk::create_text_layout(lmtk::text_layout_init{
              .renderer = renderer.get(),
              .font = font.get(),
              .position = {window_size.width / 2, window_size.height / 2},
              .text = "Hello.",
            }),
          }
    {
    }

  protected:
    void on_render(lmgl::iframe *frame) override { text_layout->render(frame); }

  private:
    lmtk::font_loader font_loader;
    lmtk::font font;
    lmtk::text_layout text_layout;
};

int main()
{
    lmtk_text_layout_example example;
    example.main();
}
