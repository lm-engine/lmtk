#pragma once

#include "font_loader.h"

namespace lmtk
{
class font_internal : public ifont
{
  public:
    font_internal(std::string file_path, FT_Face font_face);
    FT_Face get_ft_face() override;

    std::string file_path;
    FT_Face font_face;
};
} // namespace lmtk
