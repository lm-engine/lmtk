#include "font.h"
#include "font_loader.h"

namespace lmtk
{
font font_loader_internal::create_font(font_init const &init)
{
    auto font_file_path = find_font_file(init.typeface_name.c_str());

    FT_Face freetype_font_face;

    auto error =
      FT_New_Face(ft_library, font_file_path.c_str(), 0, &freetype_font_face);

    if (error)
    {
        throw std::runtime_error(
          fmt::format("Couldn't load {} with freetype.", font_file_path));
    }

    FT_Set_Pixel_Sizes(freetype_font_face, 0, init.pixel_size);

    return std::make_unique<font_internal>(font_file_path, freetype_font_face);
}

font_internal::font_internal(std::string file_path, FT_Face font_face)
    : file_path{file_path}, font_face{font_face}
{
}

FT_Face font_internal::get_ft_face() { return font_face; }
} // namespace lmtk
