#include <lmtk/font.h>

class font_internal : public lmtk::ifont
{
  public:
    FT_Face get_ft_face() override { return nullptr; }
};

class font_loader_internal : public lmtk::ifont_loader
{
  public:
    lmtk::font create_font(lmtk::font_init const &init) override
    {
        return std::make_unique<font_internal>();
    }
};

namespace lmtk
{
font_loader create_font_loader()
{
    return std::make_unique<font_loader_internal>();
}
} // namespace lmtk
