#pragma once

#include <range/v3/view/enumerate.hpp>

#include <lmlib/geometry.h>

namespace lmtk
{
struct vertical_layout
{
    lm::point2i position;
    int vertical_spacing;
};

template <typename range_type>
void layout_vertical(vertical_layout const &layout, range_type &widgets)
{
    lm::point2i current_pos{layout.position};
    for (auto [i, widget] : ranges::view::enumerate(widgets))
    {
        if (i)
            current_pos.y += (widgets.begin() + i - 1)->get_size().height +
                             layout.vertical_spacing;

        widget.set_position({current_pos.x, current_pos.y});
    }
}
} // namespace lmtk
