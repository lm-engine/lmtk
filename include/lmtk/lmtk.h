#pragma once

#include <optional>
#include <variant>

#include <boost/fusion/adapted/std_tuple.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>

#include "resource_sink.h"
#include <lmgl/lmgl.h>
#include <lmlib/geometry.h>
#include <lmlib/variant_visitor.h>
#include <lmpl/lmpl.h>
#include <range/v3/algorithm/remove.hpp>

namespace lmtk
{
struct input_state
{
    lmpl::key_state key_state;
    lmpl::mouse_state mouse_state;
    std::optional<lm::point2i> mouse_pos;

    void reset()
    {
        key_state = lmpl::key_state{};
        mouse_state = lmpl::mouse_state{};
        mouse_pos = std::nullopt;
    }
};

struct key_down_event
{
    const struct input_state &input_state;
    lmpl::key_code key;
};
struct key_up_event
{
    const struct input_state &input_state;
    lmpl::key_code key;
};
struct mouse_button_down_event
{
    const struct input_state &input_state;
    lmpl::mouse_button button;
    lm::point2i pos;
};
struct mouse_button_up_event
{
    const struct input_state &input_state;
    lmpl::mouse_button button;
    lm::point2i pos;
};
struct mouse_move_event
{
    const struct input_state &input_state;
    lm::point2i pos;
    lm::size2i delta;
};
using input_event = std::variant<
  key_down_event,
  key_up_event,
  mouse_button_down_event,
  mouse_button_up_event,
  mouse_move_event>;

/// Create input event from platform window event, updating the input state.
std::optional<input_event>
  create_input_event(const lmpl::window_message &msg, input_state &input_state);

class iwidget
{
  public:
    virtual iwidget &add_to_frame(lmgl::iframe *frame) = 0;
    virtual lm::size2i get_size() = 0;
    virtual lm::point2i get_position() = 0;
    virtual iwidget &set_rect(lm::point2i position, lm::size2i size) = 0;
    virtual iwidget &move_resources(
      lmgl::irenderer *renderer,
      resource_sink &resource_sink) = 0;
};
} // namespace lmtk
