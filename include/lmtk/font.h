#pragma once

#include <string>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <lmlib/reference.h>

namespace lmtk
{
class ifont
{
  public:
    virtual FT_Face get_ft_face() = 0;
    virtual ~ifont() = default;
};

struct font_init
{
    std::string const &typeface_name;
    unsigned pixel_size;
};

using font = lm::reference<ifont>;

class ifont_loader
{
  public:
    virtual font create_font(font_init const &init) = 0;
    virtual ~ifont_loader() = default;
};

using font_loader = lm::reference<ifont_loader>;

font_loader create_font_loader();
} // namespace lmtk
